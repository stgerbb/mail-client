package ch.ffhs.ftoop.mailclient;

import ch.ffhs.ftoop.mailclient.windows.Client;

/**
 * Main
 * 
 * @author Stefan Gerber
 * @autor Kevin Rossi
 * @since 24/05/2017
 * @version 0.6
 */
public class Main {

	/**
	 * Diese Methode wird ausgefuert beim starten der Applikation
	 * 
	 * @param args - Argumente, welche mitgegeben werden können
	 */
	public static void main(String[] args) {
		
		Client client = new Client();
		client.createJFrame();
	}
}