package ch.ffhs.ftoop.mailclient.windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import ch.ffhs.ftoop.mailclient.mail.SendMail;
import ch.ffhs.ftoop.mailclient.models.tblModel;

/**
 * Diese Klasse enthaelt Methoden zur erstellung des Clients
 * 
 * @author Stefan Gerber
 * @author Kevin Rossi
 * @since 24/05/2017
 * @version 0.6
 */
public class Client {
	
	// Fenstergroesse
	private static final int WIDTH = 800;
	private static final int HEIGHT = 600;
	
	// JFrame-Objekt
	private JFrame frame;
	
	// Standard Ordner
	private DefaultMutableTreeNode ordnerRoot;
	private DefaultMutableTreeNode newMailChild;
	private DefaultMutableTreeNode favoritenChild;
	private DefaultMutableTreeNode wichtigChild;
	private DefaultMutableTreeNode personenChild;
	private DefaultMutableTreeNode sozialeNetzwerkeChild;
	private DefaultMutableTreeNode shoppingChild;
	private DefaultMutableTreeNode reisenChild;
	private DefaultMutableTreeNode finanzenChild;
	private DefaultMutableTreeNode privatChild;
	private DefaultMutableTreeNode familieChild;
	private DefaultMutableTreeNode junkChild; 
	private DefaultMutableTreeNode notizenChild;
	
	// Tree-Modell
	private DefaultTreeModel treeModel;
	
	// JTree
	private JTree tree;
	
	/**
	 * Konstruktor
	 */
	public Client() {
		
	}
	
	/**
	 * Diese Methode erstellt das Hauptfenster des Clients
	 */
	public void createJFrame() {
		
		// init
		this.frame = new JFrame();
		
		// JFrame erstellen
		this.frame.setTitle("Mail-Client");
		this.frame.setSize(Client.WIDTH, Client.HEIGHT);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLayout(new BorderLayout());
		this.frame.setResizable(true);
		this.frame.setLocationRelativeTo(null);
		
		// Dem JFrame werden alle Komponenten hinzugefuegt
		this.frame.add(this.createNorthPanel(), BorderLayout.NORTH);
		this.frame.add(this.createCenterPanel(), BorderLayout.CENTER);
		this.frame.add(this.createSouthPanel(), BorderLayout.SOUTH);
		
		this.frame.setVisible(true);
	}
	
	/**
	 * Diese Methode erstellt eine Menubar und fuegt diese dem JFrame hinzu
	 * 
	 * @return panel - Ein JPanel wird zurueckgegeben
	 */
	public JPanel createNorthPanel() {
		
		// Menu Eintraege
		JMenu jmDatei = new JMenu("Datei");
		JMenu jmDir = new JMenu("Ordner");
		JMenu jmMail = new JMenu("E-Mail");
		
		// Submenus
		JMenuItem jmiNewMail = new JMenuItem("Neues E-Mail verfassen...");
		JMenuItem jmiNewDir = new JMenuItem("Neuer Ordner erstellen...");
		JMenuItem jmiGetMail = new JMenuItem("E-Mails abholen");
		JMenuItem jmiMailSettings = new JMenuItem("Einstellungen...");
		JMenuItem jmiExit = new JMenuItem("Beenden");
		
		// Submenus werden den Menu Eintraege hinzugefuegt
		jmDatei.add(jmiExit);
		jmDir.add(jmiNewDir);
		jmMail.add(jmiNewMail);
		jmMail.add(jmiGetMail);
		jmMail.add(new JSeparator());
		jmMail.add(jmiMailSettings);
		
		JButton btnTest = new JButton("TEST");
		
		// Menu Eintragee werden der Menubar hinzugefuegt
		JMenuBar menubar = new JMenuBar();
		menubar.add(jmDatei);
		menubar.add(jmDir);
		menubar.add(jmMail);

		// JPanel erstellen, welchem die Menubar hinzugefuegt wird
		JPanel panelN = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelN.add(menubar);
		panelN.add(btnTest);
		
		// Wenn in der Menubar unter "E-Mail" auf "E-Mail verfassen..." gedruekct wird
		jmiNewMail.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Fenster zum eine neue E-Mail zu verfassen wird geoffnet
				new CreateMailWindow().createWindow();
			}
		});
		
		// Wenn in der Menubar unter "Ordner" auf "Ordner erstellen..." gedruekct wird
		jmiNewDir.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Fenster um einen neuen Ordner zu erstellen wird geoeffnet
				addNewDirectory();
				
			}
		});
		
		// Wenn in der Menubar unter "Fenster" auf "Einstellungen" gedrueckt wird
		jmiMailSettings.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Fenster um EInstellungen anzupassen oeffnen
				new MailSettingsWindow().createWindow();
			}
		});
		
		// Wenn in der Menubar unter "E-Mail" auf "E-Mails abholen" gedrueckt wird
		jmiGetMail.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				//Todo -> Emails empfangen
				JOptionPane.showMessageDialog(null, "E-Mails werden vom Server abgeholt");
			}
		});
		
		// Wenn in der Menubar unter "Datei" auf "Beenden" gedrueckt wird
		jmiExit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
		
		btnTest.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				new SendMail().constructAndSendMail("Das ist ein Test");
			}
		});

		return panelN;
	}
	
	/**
	 * Diese Methode erstellt eine JTable und ein JTree und fuegt diese dem JFrame hinzu
	 * 
	 * @return splitPane - Ein JSplitPane wird zurueckgegeben
	 */
	public JSplitPane createCenterPanel() {
		
		// ---- JTree ---- //
		
		// init
		this.ordnerRoot = new DefaultMutableTreeNode("Meine Ablage");
		this.newMailChild = new DefaultMutableTreeNode("Neue E-Mails");
		this.favoritenChild = new DefaultMutableTreeNode("Favoriten");
		this.wichtigChild = new DefaultMutableTreeNode("Wichtig");
		this.personenChild = new DefaultMutableTreeNode("Personen");
		this.sozialeNetzwerkeChild = new DefaultMutableTreeNode("Soziale Netzwerke");
		this.shoppingChild = new DefaultMutableTreeNode("Shopping");
		this.reisenChild = new DefaultMutableTreeNode("Reisen");
		this.finanzenChild = new DefaultMutableTreeNode("Finanzen");
		this.privatChild = new DefaultMutableTreeNode("Privat");
		this.familieChild = new DefaultMutableTreeNode("Familie");
		this.junkChild = new DefaultMutableTreeNode("Junk E-Mails");
		this.notizenChild = new DefaultMutableTreeNode("Notizen");getClass();
		
		this.treeModel = new DefaultTreeModel(this.ordnerRoot);
		
		this.tree = new JTree(treeModel);
		
		// Alle Childs dem Root-Knoten "ordnerRoot" hinzufuegen
		this.ordnerRoot.add(this.newMailChild);
		this.ordnerRoot.add(this.favoritenChild);
		this.ordnerRoot.add(this.wichtigChild);
		this.ordnerRoot.add(this.personenChild);
		this.ordnerRoot.add(this.sozialeNetzwerkeChild);
		this.ordnerRoot.add(this.shoppingChild);
		this.ordnerRoot.add(this.reisenChild);
		this.ordnerRoot.add(this.finanzenChild);
		this.ordnerRoot.add(this.privatChild);
		this.ordnerRoot.add(this.familieChild);
		this.ordnerRoot.add(this.junkChild);
		this.ordnerRoot.add(this.notizenChild);
		
		// Root-Knoten expandieren
		this.tree.expandRow(0);
		
		// JTree einem JScrollPane hinzufuegen
		JScrollPane scrollPaneTr = new JScrollPane(this.tree);
	
		//---- JTable ---- //
		
		// tblModel-Objekt erstellen
		tblModel tblmodel = new tblModel();
		
		// JTable erstellen mit einem Modell
		JTable table = new JTable(tblmodel);
		
		// JTable einem ScrollPane hinzufuegen
		JScrollPane scrollPaneTbl = new JScrollPane(table);
		
		// JTable und JTree einem JSplitPane hinzufuegen
		JSplitPane splitPaneC = new JSplitPane();
		splitPaneC.setLeftComponent(scrollPaneTr);
		splitPaneC.setRightComponent(scrollPaneTbl);
		
		// Eintrag im PopupMenu
		JMenuItem jmiVerschieben = new JMenuItem("Verschieben");
		
		// Eintrag dem PopupMenu hinzufuegen
		JPopupMenu jpmMenuTbl = new JPopupMenu();
		jpmMenuTbl.add(jmiVerschieben);
		
		// MouseListener auf der Tabelle
		table.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				// Doppelklick
				if(e.getClickCount() == 2) {
					
					// Fenster um ein E-Mail zu lesen wird geoeffnet
					new OpenMailWindow().createWindow();;
					
				// Rechte Maustaste
				} else if(e.getButton() == MouseEvent.BUTTON3) {
					
					table.setComponentPopupMenu(jpmMenuTbl);
				}
			}
		});
		
		// Wenn auf das Popup "Verschieben" gedrueckt wird
		jmiVerschieben.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Fenster zum verschieben des E-Mails wird aufgerufen
				moveMailToDirectory();
			}
		});
		
		return splitPaneC;
	}
	
	/**
	 * Diese Methode erstellt eine Statusbar und fuegt diese dem JFrame hinzu
	 *  
	 * @return panel - Ein JPanel wird zurueckgegeben
	 */
	public JPanel createSouthPanel() {
		
		// Labels
		JLabel lblAutoren = new JLabel("Stefan Gerber & Kevin Rossi");
		
		// JPanel 
		JPanel panelS = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelS.add(lblAutoren);
		
		return panelS;
	}
	
	/**
	 * Diese Methode erstellt ein Fenster, um dem JTree ein neues Blatt hinzuzufuegen
	 */
	public void addNewDirectory() {
		
		// JFrame erstellen
		JFrame frame = new JFrame();
		frame.setTitle("Neuer Ordner erstellen");
		frame.setSize(328, 100);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		// Labels
		JLabel lblName = new JLabel("Ordnername: ");
		
		// Textfelder
		JTextField txtName = new JTextField(20);
		
		// Buttons
		JButton btnCreate = new JButton("Erstellen");
		
		// JPanel mitte, zum Ordnername eingeben
		JPanel panelC = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		// Position der Labels und Textfelder
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.anchor = GridBagConstraints.WEST;
		panelC.add(lblName, gbc);	
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		panelC.add(txtName, gbc);
		
		// JPanel
		JPanel panelS = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		panelS.add(btnCreate);
		
		// JPanels werden dem JFrame hinzugefuegt
		frame.add(panelC, BorderLayout.CENTER);
		frame.add(panelS, BorderLayout.SOUTH);
		
		// Wenn der Button "Erstellen" gedrueckt wird
		btnCreate.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Checkt ob das Feld leer ist oder nicht
				if(!txtName.getText().trim().equals("")) {
					
					// neuer Eintrag am Schluss im Tree "Meine Ablage
					((DefaultTreeModel) tree.getModel()).insertNodeInto(new DefaultMutableTreeNode(txtName.getText()), ordnerRoot, ordnerRoot.getChildCount());
																																							   
					// Fenster schliessen
					frame.dispose();
					
				} else {
					
					// Dialog anzeigen
					JOptionPane.showMessageDialog(null, "Bitte einen Ordnernamen eingeben");
				}
			}
		});
	}
	
	/**
	 * Diese Methode gibt die Blaetter des JTrees zurueck
	 * 
	 * @return nodes - Object Array mit allen Childs des Root-Knoten wird zurueckgegeben
	 */
	public String[] getTreeNodes() {
		
		// Root-Knoten ermitteln
		Object rootNode = this.tree.getModel().getRoot();
		
		// Anzahl Childs ermitteln
		int childCount = this.tree.getModel().getChildCount(rootNode);
		
		// Object ArrayList zum speichern der nodes
		String[] nodes = new String[childCount];
		
		// Alle Childs des Root-Knotens im Array "nodes" speichern
		for(int i = 0; i < childCount; i++) {
			
			nodes[i] = this.tree.getModel().getChild(rootNode, i).toString();
		}
		
		return nodes;
	}
	
	/**
	 * Diese Methode erstellt ein Fenster zum verschieben eines E-Mails in ein Ordner
	 */
	public void moveMailToDirectory() {
		
		// JFrame erstellen
		JFrame frame = new JFrame();
		frame.setTitle("E-Mail in Ordner verschieben");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(400, 400);
		frame.setLayout(new BorderLayout());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		
		// JButton
		JButton btnVerschieben = new JButton("Verschieben");
		
		// JList mit allen Ordner
		JList<String> ordnerList = new JList<String>(this.getTreeNodes());
		
		// JList wird einem JScrollPane hinzugefuegt
		JScrollPane scrollPaneOrdner = new JScrollPane(ordnerList);
		
		// ScrollPane wird einem JPanel hinzugefuegt
		JPanel panelC = new JPanel(new BorderLayout());
		panelC.add(scrollPaneOrdner);
		
		// JPanel unten
		JPanel panelS = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panelS.add(btnVerschieben);
		
		// Panels werdem dem JFrame hinzugefuegt
		frame.add(panelC, BorderLayout.CENTER);
		frame.add(panelS, BorderLayout.SOUTH);
		
		// Wenn auf den Button "Verschieben" gedrueckt wird
		btnVerschieben.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// TODO -> Mail in Ordner Verschieben
				
				//Fenster schliessen
				frame.dispose();
			}
		});
		
		frame.setVisible(true);
	}
	
	public JFrame getFrame() {
		
		return this.frame;
	}
}