package ch.ffhs.ftoop.mailclient.windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Diese Klasse stellt ein Fenster dar, wenn ein Mail geoeffnet wird
 * 
 * @author Stefan Gerber
 * @author Kevin Rossi
 * @since 25/05/2017
 * @version 0.6
 */
public class OpenMailWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * Konstruktor
	 */
	public OpenMailWindow() {
		
	}
	
	/**
	 * Diese Methode erstellt das Fenster
	 */
	public void createWindow() {
		
		// JFrame erstellen
		setTitle("E-Mail");
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		setResizable(true);
		setLocationRelativeTo(null);
		
		// Komponenten dem JFrame hinzufuegen
		this.addComponentsToJFrame();
		
		setVisible(true);	
	}
	
	/**
	 * Diese Methode fuegt Komponenten dem JFrame hinzu
	 */
	public void addComponentsToJFrame() {
		
		// Labels
		JLabel lblSpaceT = new JLabel("");
		JLabel lblAktion = new JLabel("");
		JLabel lblVon = new JLabel("Von:");
		JLabel lblAn = new JLabel("An:");
		JLabel lblCC = new JLabel("CC:");
		JLabel lblBCC = new JLabel("BCC:");
		JLabel lblSubject = new JLabel("Betreff:");
		JLabel lblSpaceB = new JLabel("");
		
		// Textfelder
		JTextField txtAn = new JTextField(40);
		JTextField txtVon = new JTextField(40);
		JTextField txtCC = new JTextField(40);
		JTextField txtBCC = new JTextField(40);
		JTextField txtSubject = new JTextField(40);
		
		// Buttons
		JButton btnSchliessen = new JButton("Schliessen");

		// Grosses Textfeld
		JTextArea txtMessage = new JTextArea();
		
		// ComboBox
		JComboBox<String> cmbAktion = new JComboBox<String>(new String[] {"Aktion auswählen...", "Antworten", "Weiterleiten"});

		// JPanel oben, welches Von, An, CC, BCC und Betreff enthaelt 
		JPanel panelN = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		// Position der Labels und Textfelder: Von, An, CC, BCC und Betreff
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.anchor = GridBagConstraints.WEST;
		panelN.add(lblSpaceT, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		panelN.add(lblAktion, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelN.add(cmbAktion, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelN.add(lblVon, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		panelN.add(txtVon, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelN.add(lblAn, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		panelN.add(txtAn, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelN.add(lblCC, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelN.add(txtCC, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelN.add(lblBCC, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 5;
		panelN.add(txtBCC, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 6;
		panelN.add(lblSubject, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 6;
		panelN.add(txtSubject, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 7;
		panelN.add(lblSpaceB, gbc);
		
		// JPanel mitte, welches die Nachricht enthaelt
		JScrollPane scrollPaneC = new JScrollPane(txtMessage);
		JPanel panelC = new JPanel(new BorderLayout());
		panelC.add(scrollPaneC, BorderLayout.CENTER);
		
		// JPanel unten, welches die Buttons enthaelt
		JPanel panelS = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		panelS.add(btnSchliessen);
		
		// Panels werden dem JFrame hinzugefuegt
		add(panelN, BorderLayout.NORTH);
		add(panelC, BorderLayout.CENTER);
		add(panelS, BorderLayout.SOUTH);
		
		// Wenn in der ComboBox auf ein Item geklickt wird
		cmbAktion.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Reaktion auf Itemselektion der ComboBox
				Object selectedItem = cmbAktion.getSelectedItem();
				switch(selectedItem.toString()) {
					
					case "Antworten": new CreateMailWindow().createWindow(); // Fenster zum verfassen einer neuer E-Mail wird geoeffnet
					break;
					
					case "Weiterleiten": new CreateMailWindow().createWindow(); // Fenster zum verfassen einer neuer E-Mail wird geoeffnet
					break;
				}
			}
		});
		
		// Wenn auf den Button "Schliessen" gedrueckt wird
		btnSchliessen.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				// Fenster schliessen
				dispose();
			}
		});
	}
}