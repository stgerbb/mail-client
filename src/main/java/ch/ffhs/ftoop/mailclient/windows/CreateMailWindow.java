package ch.ffhs.ftoop.mailclient.windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Diese Klasse stellt ein Fenster dar, wenn ein Mail verfasst wird
 * 
 * @author Stefan Gerber
 * @author Kevin Rossi
 * @since 25/05/2017
 * @version 0.6
 */
public class CreateMailWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * Konstruktor
	 */
	public CreateMailWindow() {
		
	}
	
	/**
	 * Diese Methode erstellt das Fenster
	 */
	public void createWindow() {
		
		// JFrame erstellen
		setTitle("Neue E-Mail verfassen");
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		setResizable(true);
		setLocationRelativeTo(null);
		
		// Komponenten dem JFrame hinzufuegen
		this.addComponentsToJFrame();
		
		setVisible(true);
	}
	
	/**
	 * Diese Methode fuegt Komponenten dem JFrame hinzu
	 */
	public void addComponentsToJFrame() {
		
		// Labels
		JLabel lblVon = new JLabel("Von:");
		JLabel lblAn = new JLabel("An:");
		JLabel lblCC = new JLabel("CC:");
		JLabel lblBCC = new JLabel("BCC:");
		JLabel lblSubject = new JLabel("Betreff:");
		JLabel lblSpaceT = new JLabel("");
		JLabel lblSpaceB = new JLabel("");
		
		// Textfelder
		JTextField txtAn = new JTextField(45);
		JTextField txtVon = new JTextField(45);
		JTextField txtCC = new JTextField(45);
		JTextField txtBCC = new JTextField(45);
		JTextField txtSubject = new JTextField(45);
		
		// txtVon nicht editierbar
		txtVon.setEditable(false);;;
		
		// E-Mail Adresse erhalten vom EInstellungen Fenster
		MailSettingsWindow optionWindow = new MailSettingsWindow();
		txtVon.setText(optionWindow.getEMail());

		// Grosses Textfeld
		JTextArea txtMessage = new JTextArea();
		
		// Buttons
		JButton btnSend = new JButton("Senden");
		JButton btnCancel = new JButton("Verwerfen");
		
		// JPanel oben, welches Von, An, CC, BCC und Betreff enthaelt 
		JPanel panelN = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		// Position der Labels und Textfelder: Von, An, CC, BCC und Betreff
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.anchor = GridBagConstraints.WEST;
		panelN.add(lblSpaceT, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		panelN.add(lblVon, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelN.add(txtVon, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelN.add(lblAn, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		panelN.add(txtAn, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelN.add(lblCC, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		panelN.add(txtCC, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelN.add(lblBCC, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelN.add(txtBCC, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelN.add(lblSubject, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 5;
		panelN.add(txtSubject, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 6;
		panelN.add(lblSpaceB, gbc);
	
		// JPanel mitte, welches die Nachricht enthaelt
		JScrollPane scrollPaneC = new JScrollPane(txtMessage);
		JPanel panelC = new JPanel(new BorderLayout());
		panelC.add(scrollPaneC, BorderLayout.CENTER);
		
		// JPanel unten, welches die Buttons enthaelt
		JPanel panelS = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		panelS.add(btnSend);
		panelS.add(btnCancel);
		
		// Panels werden dem JFrame hinzugefuegt
		add(panelN, BorderLayout.NORTH);
		add(panelC, BorderLayout.CENTER);
		add(panelS, BorderLayout.SOUTH);
		
		// Wenn auf den Button "Abbrechen" gedrueckt wird
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Selection erhalten
				int selection = JOptionPane.showOptionDialog(null, "Die aktuelle E-Mail verwerfen?", "Warnung", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
				if(selection == JOptionPane.YES_OPTION) {
					
					// Fenster schliessen
					dispose();
				} 
			}	
		});
		
		// Wenn auf den Button "Senden" gedrueckt wird
		btnSend.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Folgende Felder muessen ausgefuellt sein
				if(txtVon.getText().trim().equals("") || txtAn.getText().trim().equals("") || txtMessage.getText().trim().equals("") || txtSubject.getText().trim().equals("")) {
					
					JOptionPane.showMessageDialog(null, "Stellen Sie sicher, dass sie folgende Felder ausgefüllt haben:\n\n Von\n An\n Betreff\n Nachricht");
					
				} else {
					
					// TODO -> E-Mail versenden
					
					// Fenster schliessen
					dispose();
				}
			}
		});
	}
}