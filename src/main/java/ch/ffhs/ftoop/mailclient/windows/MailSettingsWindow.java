package ch.ffhs.ftoop.mailclient.windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import ch.ffhs.ftoop.mailclient.mail.ReceiveMail;
import ch.ffhs.ftoop.mailclient.mail.SendMail;

/**
 * Diese Klasse stellt ein Fenster für die Einstellungen dar
 * 
 * @author Stefan Gerber
 * @author Kevin Rossi
 * @since 25/05/2017
 * @version 0.6
 */
public class MailSettingsWindow extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private SendMail sm;
	private ReceiveMail rm;
	
	private JTextField txtEmail;
	
	/**
	 * Konstruktor
	 */
	public MailSettingsWindow() {
		
		// init
		
		this.sm = new SendMail();
		this.txtEmail = new JTextField(40);
		this.txtEmail.setText(sm.getFrom());
	}	
	
	/**
	 * Diese Methode erstellt das Fenster
	 */
	public void createWindow() {
		
		//JFrame erstellen
		setTitle("Einstellungen");
		setSize(600, 310);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		setResizable(false);
		setLocationRelativeTo(null);
		
		// Komponenten dem JFrame hinzufuegen
		addComponentsToJFrame();

		setVisible(true);
	}
	
	/**
	 * Komponenten werden dem JFrame hinzugefuegt
	 */
	public void addComponentsToJFrame() {
		
		// init
		this.rm = new ReceiveMail();
		
		// Labels
		JLabel lblSpaceT = new JLabel("");
		JLabel lblPop3 = new JLabel("Posteingangsserver:");
		JLabel lblEmail = new JLabel("E-Mail Adresse:");
		JLabel lblUsername = new JLabel("Benutzername:");
		JLabel lblPassword = new JLabel("Passwort:");
		JLabel lblPortPop3 = new JLabel("Port POP3:");
		JLabel lblPortSmtp = new JLabel("Port SMTP:");
		JLabel lblSmtp = new JLabel("Postausgangsserver:");
		
		
		// Textfelder
		JTextField txtPop3 = new JTextField(40);
		JTextField txtUsername = new JTextField(40);
		JTextField txtSmtp = new JTextField(40);
		JTextField txtPortSmtp = new JTextField(40);
		JTextField txtPortPop3 = new JTextField(40);
		JPasswordField txtPassword = new JPasswordField(40);
		
		// Textfeleder befuellen
		txtUsername.setText(sm.getUsername());
		txtPassword.setText(sm.getPassword());
		txtSmtp.setText(sm.getHost());
		txtPortSmtp.setText(sm.getPort());
		txtPortPop3.setText(rm.getPort());
		txtPop3.setText(rm.getHost());
		
		// Textfelder welche nicht editierbar sind
		txtPop3.setEditable(false);
		txtSmtp.setEditable(false);
		txtPortSmtp.setEditable(false);
		txtPortPop3.setEditable(false);
		
		// Buttons
		JButton btnSave = new JButton("Speichern");
		JButton btnAbort = new JButton("Abbrechen");
		
		// JPanel oben, welches JLabels enthaelt
		JPanel panelN = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		//Position der Label und Textfelder: POP3, E-Mail Adresse, Username, Passwort
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.anchor = GridBagConstraints.WEST;
		panelN.add(lblSpaceT, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		panelN.add(lblEmail, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		panelN.add(txtEmail, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		panelN.add(lblUsername, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		panelN.add(txtUsername, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		panelN.add(lblPassword, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		panelN.add(txtPassword, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		panelN.add(new JSeparator(), gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		panelN.add(new JSeparator(), gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		panelN.add(lblPop3, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 5;
		panelN.add(txtPop3, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 6;
		panelN.add(lblPortPop3, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 6;
		panelN.add(txtPortPop3, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 7;
		panelN.add(new JSeparator(), gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 7;
		panelN.add(new JSeparator(), gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 8;
		panelN.add(lblSmtp, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 8;
		panelN.add(txtSmtp, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 9;
		panelN.add(lblPortSmtp, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 9;
		panelN.add(txtPortSmtp, gbc);
		
		
		// JPanel unten, welches die Buttons enthaelt
		JPanel panelS = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		panelS.add(btnSave);
		panelS.add(btnAbort);
		
		// JPanels werden dem JFrame hinzugefuegt
		add(panelN, BorderLayout.NORTH);
		add(panelS, BorderLayout.SOUTH);
		
		// Wenn auf den Button Speichern gedrueckt wird
		btnSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Folgende Felder muessen ausgefuellt sein
				if(txtUsername.getText().trim().equals("") || txtPassword.getText().trim().equals("") || txtEmail.getText().trim().equals("") || txtPop3.getText().equals("")) {
					
					JOptionPane.showMessageDialog(null, "Stellen Sie sicher, dass sie folgende Felder ausgefüllt haben:\n\n User Name\n Passwort\n E-Mail Adresse\n POP3");
					
				} else {
					
					// Username, Passwort, Email, POP3 Einstellungen speichern
					sm.setPassword(txtPassword.getText().trim()); // Aus Sicherheitsgruenden muesste man .getPassword() verwenden, jedoch spielt Sicherheit bei diesem Programm keine Rolle
					sm.setUsername(txtUsername.getText().trim());
					sm.setFrom(txtEmail.getText().trim());
					rm.setHost(txtPop3.getText().trim());
					
					// Fenster schliessen
					dispose();
				}
			}	
		});
		
		// Wenn auf den Button Abbrechen gedrueckt wird
		btnAbort.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Selection erhalten
				int selection = JOptionPane.showOptionDialog(null, "Wollen Sie wirklich abbrechen? \nDie vorgenommenen Änederungen gehen verloren", "Warnung", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
				if(selection == JOptionPane.YES_OPTION) {
					
					// Fenster schliessen
					dispose();
				}
			}
		});
	}
	
	// ---- Getters ---- //
	/**
	 *  Diese Methode gibt den Inhalt des txtEmail zurueck
	 *  
	 * @return this.txtEmail.getText() - Inhalt wird als String zurueckgegeben
	 */
	public String getEMail() {
		
		return this.txtEmail.getText();
	}
}