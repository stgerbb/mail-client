package ch.ffhs.ftoop.mailclient.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.swing.JOptionPane;

/**
 * Diese Klasse ist fuer das Senden einer E-Mail zustaendig
 * 
 * @since 25/05/2017
 * @version 0.6
 */
public class SendMail {
	
	// Empfänger Mail-Adresse
	private String to;
	
	// Sender Mail-Adresse
	private String from;
	
	// Google Konto Infos
	private String username;
	private String password;
	
	// SMTP Einstellungen
	private String host;
	private String port;
	
	/**
	 * Konstruktor
	 */
	public SendMail() {
		
		// init
		this.to = "ftoop.ffhs.2017@gmail.com" ;
		this.from = "ftoop.ffhs.2017@gmail.com" ;
		
		this.username = "ftoop.ffhs.2017@gmail.com" ;
		this.password = "Pibs$2017";
		
		this.host = "smtp.gmail.com";
		this.port = "587";
	}
	
	/**
    * Diese Methode Konstruiert und sendet ein E-Mail
    *
    * @param nachricht - Inhalt des Mails
    */
   public void constructAndSendMail(String nachricht) {    
     
      // Systemeinstellungen
      Properties properties = System.getProperties() ;

      // Setup Einstellungen für den Mail Server
      properties.setProperty( "mail.smtp.auth",            "true"           ) ;
      properties.setProperty( "mail.smtp.starttls.enable", "true"           ) ;
      properties.setProperty( "mail.smtp.host",            this.host 		) ;
      properties.setProperty( "mail.smtp.port",            this.port        ) ;

      // Get standard Session Objekt
      Session session = Session.getDefaultInstance( properties ) ;

      try {
    	  
         // kreiert ein default MimeMessage Object
         MimeMessage message = new MimeMessage( session ) ;

         // Set From: Header Feld im Header
         message.setFrom( new InternetAddress(this.from) ) ;

         // Set To: Header Feld im Header
         message.addRecipient( Message.RecipientType.TO, new InternetAddress(this.to) ) ;
         
         // Set CC und BCC
         message.setRecipient(RecipientType.CC, new InternetAddress("example1@example.com"));
         message.setRecipient(RecipientType.BCC, new InternetAddress("example2@example.com"));

         // Set Subject: Header Feld im Header
         message.setSubject( "Testmail" ) ;

         // Aktuelle Nachricht wird gesetzt
         message.setText( nachricht ) ;

         Transport transport = session.getTransport( "smtps" ) ;

         try {
        	 
            transport.connect( this.host, this.username, this.password ) ;
            transport.sendMessage( message, message.getAllRecipients() ) ;
            
            
         } finally {
        	 
            transport.close() ;
         }

         JOptionPane.showMessageDialog(null, "E-Mail wurde gesendet");
         
      } catch( MessagingException e ) {
    	  
         e.printStackTrace() ;
      }
   }
	
   // ---- Getters und Setters ---- //
   /**
    * Diese Methode gibt den Empfaenger zurueck
    * 
    * @return this.to - Empfaenger wird zurueckgegeben
    */
   public String getTo() {
	   
		return this.to;
	}
   
   /**
    * Diese Methode setzt einen neuen Empfaenger
    * 
    * @param from - Neuer Empfaenger eingeben
    */
   	public void setTo(String to) {
	   
		this.to = to;
	}

   /**
    * Diese Methode gibt den Sender zurueck
    * 
    * @return this.from - Sender wird zurueckgegeben
    */
    public String getFrom() {
		
		return this.from;
	}
    
    /**
     * Diese Methode setzt einen neuen Sender
     * 
     * @param from - Neuer Sender eingeben
     */
    public void setFrom(String from) {
		
		this.from = from;
	}
    
    /**
     * Diese Methode gibt den Benutzernamen zurueck
     * 
     * @return this.username - Benutzername wird zurueckgegeben
     */
	public String getUsername() {
		
		return this.username;
	}
	
	 /**
     * Diese Methode setzt einen neuen Benutzernamen
     * 
     * @param username - Neuer Benutzername eingeben
     */
	public void setUsername(String username) {
		
		this.username = username;
	}
	
	 /**
     * Diese Methode gibt das Passwort zurueck
     * 
     * @return this.password - Passwort wird zurueckgegeben
     */
	public String getPassword() {
		
		return this.password;
	}
	
	 /**
     * Diese Methode setzt ein neues Passwort
     * 
     * @param password - Neues Password eingeben
     */
	public void setPassword(String password) {
		
		this.password = password;
	}
	
	/**
    * Diese Methode setzt ein neuer SMTP Port
    * 
    * @param port - Neuer SMTP Port eingeben
    */
	public void setPort(String port) {
		
		this.port = port;
	}	
	
	/**
     * Diese Methode gibt den SMTP Port zurueck
     * 
     * @return this.port - SMTP Port wird zurueckgegeben
     */
	public String getPort() {
		
		return this.port;
	}
	
	/**
     * Diese Methode gibt den SMTP Host zurueck
     * 
     * @return this.host - SMTP Host wird zurueckgegeben
     */
	public String getHost() {
		
		return this.host;
	}
	
	/**
	* Diese Methode setzt einen neuen SMTP Host
	* 
	* @param host - Neuen SMTP Host eingeben
	*/
	public void setHost(String host) {
		
		this.host = host;
	}
}