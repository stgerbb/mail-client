package ch.ffhs.ftoop.mailclient.mail;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

/**
 * Diese Klasse ist für das Empfangen von E-Mails zustaendig
 * 
 * @since 25/05/2017
 * @version 0.6
 */
public class ReceiveMail {
	
	// Google Konto Info
    private String user;
    private String password;
    
    // DiePort und IP-Adresse des POP3 Server
    private String host;
    private String port;
	
	public ReceiveMail() {
		
		// init
		this.user     = "ftoop.ffhs.2017@gmail.com" ;
		this.password = "Pibs$2017" ;
		
		this.host = "pop.gmail.com" ;
		this.port = "995";
	}
	
	/**
    * Diese Methode empfaengt ein E-Mail
    */
   public void receiveMail() {      

      // Get Systemeinstellungen
      Properties properties = System.getProperties() ;
      
      // Anfrage POP3 Server
      properties.put( "mail.store.protocol", "pop3s" ) ;

      // Get standard Session Objekt
      Session session = Session.getDefaultInstance( properties ) ;

      try {
         // Get a store for the POP3S protocol
         Store store = session.getStore() ;

         // Zum momentanen Host verbinden mit spezifiziertem Username und Passwort
         store.connect( host, this.user, this.password ) ;

         // Ein Ordner Objekt mit angegebenen Namen erstellen
         Folder folder = store.getFolder( "inbox" ) ;

         // Ordner oeffnen
         folder.open( Folder.READ_WRITE ) ;

         // Die Nachricht vom Server bekommen
         Message[] messages = folder.getMessages();

         // Nachricht anzeigen
         for( int i = 0; i < messages.length; i++ ) {
            Message msg = messages[i] ;

            String from = InternetAddress.toString( msg.getFrom() ) ;
            if( from != null )
            {
               System.out.println( "From: " + from ) ;
            }

            String to = InternetAddress.toString( msg.getRecipients(Message.RecipientType.TO) ) ;
            if( to != null ) {
               System.out.println( "To: " + to ) ;
            }

            String subject = msg.getSubject() ;
            if( subject != null ) {
               System.out.println( "Subject: " + subject ) ;
            }

            Date sent = msg.getSentDate() ;
            if( sent != null ) {
               System.out.println( "Sent: " + sent ) ;
            }

            // Leere Zeile um den Header vom Body zu trennen
            System.out.println() ;

            // Das kann zu Problemen führen, wenn etwas anderes als Text gesendet wurde
            System.out.println( msg.getContent() ) ;

            /* In der endgueltigen Version sollen die Mails geloescht werden */

            // Mark this message for deletion when the session is closed
            // msg.setFlag( Flags.Flag.DELETED, true ) ;
         }

         folder.close( true ) ;
         store.close() ;
         
      } catch( MessagingException | IOException e ) {
    	  
         e.printStackTrace() ;
      }
    }

   	// ---- Getters und Setters ---- //
   /**
    * Diese Methode gibt den Benutzer zurueck
    * 
    * @return this.user - Benutzername wird zurueckgegeben
    */
	public String getUser() {
		
		return this.user;
	}
	
	/**
    * Diese Methode setzt einen neuen Benutzer
    * 
    * @param user - Neuer Benutzernamen eingeben
    */
	public void setUser(String user) {
		
		this.user = user;
	}
	
	/**
    * Diese Methode gibt das Passwort zurueck
    * 
    * @return this.password - Passwort wird zurueckgegeben
    */
	public String getPassword() {
		
		return this.password;
	}
	
	/**
    * Diese Methode setzt ein neues Passwort
    * 
    * @param password - Neues Password eingeben
    */
	public void setPassword(String password) {
		
		this.password = password;
	}
	
	/**
    * Diese Methode gibt den POP3 Host zurueck
    * 
    * @return this.user - POP3 Host wird zurueckgegeben
    */
	public String getHost() {
		
		return this.host;
	}
	
	/**
    * Diese Methode setzt einen neuen POP3 Host
    * 
    * @param host - Neuen POP3 Host angeben
    */
	public void setHost(String host) {
		
		this.host = host;
	}
	
	/**
    * Diese Methode gibt den POP3 Port zurueck
    * 
    * @return this.port - POP3 Port wird zurueckgegeben
    */	
	public String getPort() {
		
		return this.port;
	}
	
	/**
	* Diese Methode setzt einen neuen POP3 Port
	* 
	* @param port - Neuen POP3 Port eingeben
	*/
	public void setPort(String port) {
		
		this.port = port;
	}
}