package ch.ffhs.ftoop.mailclient.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.AbstractTableModel;

/**
 * Diese Klasse enthaelt Methoden, um ein Tabellen Modell zu erstellen
 * 
 * @author Stefan Gerber
 * @author Kevin Rossi
 * @since 24/05/2017
 * @version 0.6
 */
public class tblModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	// Spaltennamen
	private static final String[] COLUMN_NAMES = {"Sender", "Betreff", "Nachricht", "Datum/Uhrzeit"};
	
	// ArrayListe für die Zeilen der Tabelle
	private ArrayList<String[]> rows;
	
	public tblModel() {
		
		// init
		this.rows = new ArrayList<String[]>();
		
		// Datum und Uhrzeit
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		Date date = new Date();
		
		// Tabelle befuellen
		this.rows.add(new String[] {"example@example.com", "Betreff hier", "Nachricht hier", dateFormat.format(date) });
	}
	
	/**
	 * Diese Methode gibt die Anzahl der Zeilen in der Tabelle zurueck
	 */
	@Override
	public int getRowCount() {
		
		return this.rows.size();
	}
	
	/**
	 * Diese Methode gibt die Anzahl der Spalten in der Tabelle zurueck
	 */
	@Override
	public int getColumnCount() {
		
		String[] row = this.rows.get(0);
		
		return row.length;
	}
	
	/**
	 * Diese Methode gibt ein Element in der Tabelle zurueck
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		String[] row = this.rows.get(rowIndex);
		
		return row[columnIndex];
	}
	
	/**
	 * Diese Methode gibt die Spaltennamen zurueck
	 */
	@Override
	public String getColumnName(int columnIndex) {
		
		return tblModel.COLUMN_NAMES[columnIndex];
	}
}